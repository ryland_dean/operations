{{extend 'layout.html'}}

{{block content-header}}
    <section class="content-header filled-white md-padding-bottom clearfix">
        <h1 class="sm-padding-right col-sm-12">
             Companies Overview
        </h1>
    </section>
{{end}}

{{block content}}
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="col-sm-3 col-md-4">
                    <h3 class="box-title">Companies</h3>
                </div>
                <div class="col-sm-6 col-md-4 col-md-offset-2">
                    <div class="form-group">
                        <input type="text" class="form-control" id="companySearch" oninput=searchCompanies(this); placeholder="Search by company name">
                    </div>
                </div>
                <div class="col-sm-3 col-md-2">
                    <button class="btn-flat btn-sm bg-green btn add-company pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Company</button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="overflow:hidden;height:100%;padding:0;margin:10px">
                <div class="company-box" style="overflow-y:scroll;width:calc(100% + 17px);">
                    <table class="table table-bordered">
                    <tbody id="companyTable">
                        <tr>
                            <th style="width:5%;">#</th>
                            <th style="width:30%;">Company Name</th>
                            <th style="width:20%;">Company Phone</th>
                            <th style="width:40%;">Company Address</th>
                            <th style="width:5%;">Edit</th>
                        </tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add Company -->
<div class="modal fade" id="addCompany" role="dialog" aria-labelledby="addCompany">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header clearfix">
                <h4 class="modal-title">Add a Company</h4>
            </div>
            <div class="modal-body add-company-area clearfix">
                <form class="form" id="addCompanyForm">
                    <div class="form-group">
                        <label>Company Name:</label>
                        <input type="text" name="company-name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Company Phone Number:</label>
                        <input type="phone" name="company-phone" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Company Fax Number:</label>
                        <input type="phone" name="company-fax" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Company Street Address:</label>
                        <input type="text" name="company-address" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Company City:</label>
                        <input type="text" name="company-city" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Company Province/State:</label>
                        <input type="text" name="company-state" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Company Postal Code/ZIP:</label>
                        <input type="text" name="company-zip" class="form-control">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat btn-sm" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-default btn-flat bg-green btn-sm" id="submitNewCompany" onclick="submitNewCompany()">Add Company</button>
            </div>
        </div>
    </div>
</div>

<!-- Edit Company -->
<div class="modal fade" id="editCompany" role="dialog" aria-labelledby="editCompany">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header clearfix">
                <h4 class="modal-title">Edit Company</h4>
            </div>
            <div class="modal-body edit-company-area clearfix">
                <form class="form" id="editCompanyForm">
                    <input type="hidden" name="company-id" class="form-control">
                    <div class="form-group">
                        <label>Company Name:</label>
                        <input type="text" name="company-name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Company Phone Number:</label>
                        <input type="phone" name="company-phone" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Company Fax Number:</label>
                        <input type="phone" name="company-fax" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Company Street Address:</label>
                        <input type="text" name="company-address" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Company City:</label>
                        <input type="text" name="company-city" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Company Province/State:</label>
                        <input type="text" name="company-state" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Company Postal Code/ZIP:</label>
                        <input type="text" name="company-zip" class="form-control">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat btn-sm" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-default btn-flat bg-green btn-sm" id="submitEditCompany" onclick="submitEditCompany(this)">Save Changes</button>
            </div>
        </div>
    </div>
</div>
{{end}}

{{block custom_js}}
<script>
    $(document).ready(function(){
        var boxHei = getBoxHeight(70);
        $('.company-box').css('max-height',boxHei);
    });
    
    function getCompanies(){
        var loading = '<tr class="loading-companies"><td colspan="6">Loading Companies...</td></tr>';
        $('#companyTable').append(loading);
        $.ajax({
            method:'{{=ajax_mode}}',
            url:"{{=URL(c='plugin_companies',f='all_companies')}}",
            data:{},
            success: function(data){
                $('#companyTable').find('.loading-companies').remove();
                $.each($.parseJSON(data), function(key,companies){
                    var company = '<tr class="company"><td>'+ companies.id +'</td><td style="line-height:30px;"><a class="company-name">'+ companies.company_name +'</a></td><td style="line-height:30px;">'+ companies.phone +'</td><td style="line-height:30px;">'+ companies.address +', '+ companies.city +', '+ companies.province +' '+ companies.postal +'</td><td><button data-toggle="modal" data-target="#editCompany" data-company-id="'+ companies.id +'" data-company-name="'+ companies.company_name +'" data-company-phone="'+ companies.phone +'" data-company-address="'+ companies.address +'" data-company-city="'+ companies.city +'" data-company-province="'+ companies.province +'" data-company-postal="'+ companies.postal +'" onclick="poulateEditForm(this)" class="btn btn-sm bg-red"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</button></td></tr>';
                    $('#companyTable').append(company);
                });
            },error: function(data){
                console.log(data);
                var error = '<tr><td style="width:100%;">Unable to load companies.</td></tr>';
                $('#companyTable').html(error);
            }
        });
    };

    getCompanies();

    function searchCompanies(input){
        var val = $(input).val().toLowerCase();
        var companies = $('.company-name');
        $(companies).each(function(i, obj) {
            var isContains = $(this).text().toLowerCase().indexOf(val) > -1;
            if (isContains != true){
                $(this).closest('.company').hide();
            }else{
                $(this).closest('.company').show();
            }
        });
    };
    
    function sendNewCompany(form,cname,cphone,cfax,caddress,ccity,cstate,czip){
        $.ajax({
            method:'{{=ajax_mode}}',
            url:"{{=URL(c='plugin_companies',f='addCompany')}}",
            data:{'name':cname,'phone':cphone,'fax':cfax,'address':caddress,'city':ccity,'state':cstate,'czip':czip},
            success: function(data){
                if(data=='True'){
                    var message = '<div class="callout callout-success"><p>Company has been added!</p></div>';
                    $('#submitNewCompany').text('Submitted');
                }else{
                    var message = '<div class="callout callout-danger"><p>Error adding company. Try again later.</p></div>';
                    $('#submitNewCompany').text('Errored').removeClass('bg-green').addClass('bg-red');
                }
                $('.add-company-area').prepend(message);
                $('.add-company-area').find('.callout').delay(5000).slideUp('slow');
                location.reload(true);
            },error: function(data){
                var message = '<div class="callout callout-danger"><p>Error adding company. Try again later.</p></div>';
                $('#submitNewCompany').text('Errored').removeClass('bg-green').addClass('bg-red');
                $('.add-company-area').prepend(message);
                $('.add-company-area').find('.callout').delay(5000).slideUp('slow');
                $('#submitNewCompany').delay(5000).text('Add Company').removeClass('bg-red').addClass('bg-green').prop('disabled',false);
            }
        });
    };
    
    function updateCompanyInfo(form,cid,cname,cphone,cfax,caddress,ccity,cstate,czip){
        $.ajax({
            method:'{{=ajax_mode}}',
            url:"{{=URL(c='plugin_companies',f='editCompany')}}",
            data:{'cid':cid,'name':cname,'phone':cphone,'fax':cfax,'address':caddress,'city':ccity,'state':cstate,'czip':czip},
            success: function(data){
                if(data=='True'){
                    var message = '<div class="callout callout-success"><p>Company has been edited!</p></div>';
                    $('#submitEditCompany').text('Saved');
                }else{
                    var message = '<div class="callout callout-danger"><p>Error editing company. Try again later.</p></div>';
                    $('#submitEditCompany').text('Errored').removeClass('bg-green').addClass('bg-red');
                }
                $('.edit-company-area').prepend(message);
                $('.edit-company-area').find('.callout').delay(5000).slideUp('slow');
                location.reload(true);
            },error: function(data){
                var message = '<div class="callout callout-danger"><p>Error editing company. Try again later.</p></div>';
                $('#submitEditCompany').text('Errored').removeClass('bg-green').addClass('bg-red');
                $('.edit-company-area').prepend(message);
                $('.edit-company-area').find('.callout').delay(5000).slideUp('slow');
                $('#submitEditCompany').delay(5000).text('Add Company').removeClass('bg-red').addClass('bg-green').prop('disabled',false);
            }
        });
    };
    
    function submitNewCompany(){
        $('#submitNewCompany').text('Submitting...').prop('disabled',true);
        var form = $('#addCompanyForm');
        var cid = $(form).find('input[name="company-id"]').val();
        var cname = $(form).find('input[name="company-name"]').val();
        var cphone = $(form).find('input[name="company-phone"]').val();
        var cfax = $(form).find('input[name="company-fax"]').val();
        var caddress = $(form).find('input[name="company-address"]').val();
        var ccity = $(form).find('input[name="company-city"]').val();
        var cstate = $(form).find('input[name="company-state"]').val();
        var czip = $(form).find('input[name="company-zip"]').val();
        sendNewCompany(form,cname,cphone,cfax,caddress,ccity,cstate,czip);
    };
    
    $('.add-company').click(function(){
        $('#addCompany').modal('show');
    });
    
    function poulateEditForm(btn){
        var cId = $(btn).attr('data-company-id');
        var cName = $(btn).attr('data-company-name');
        var cPhone = $(btn).attr('data-company-phone');
        var cAddress = $(btn).attr('data-company-address');
        var cCity = $(btn).attr('data-company-city');
        var cProvince = $(btn).attr('data-company-province');
        var cPostal = $(btn).attr('data-company-postal');
        var form = $('#editCompanyForm');
        $(form).find('input[name="company-id"]').val(cId);
        $(form).find('input[name="company-name"]').val(cName);
        $(form).find('input[name="company-phone"]').val(cPhone);
        $(form).find('input[name="company-address"]').val(cAddress);
        $(form).find('input[name="company-city"]').val(cCity);
        $(form).find('input[name="company-state"]').val(cProvince);
        $(form).find('input[name="company-zip"]').val(cPostal);
    };
    
    function submitEditCompany(btn){
        $(btn).text('Saving...').prop('disabled',true);
        var form = $('#editCompanyForm');
        var cid = $(form).find('input[name="company-id"]').val();
        var cname = $(form).find('input[name="company-name"]').val();
        var cphone = $(form).find('input[name="company-phone"]').val();
        var cfax = $(form).find('input[name="company-fax"]').val();
        var caddress = $(form).find('input[name="company-address"]').val();
        var ccity = $(form).find('input[name="company-city"]').val();
        var cstate = $(form).find('input[name="company-state"]').val();
        var czip = $(form).find('input[name="company-zip"]').val();
        updateCompanyInfo(form,cid,cname,cphone,cfax,caddress,ccity,cstate,czip);
    };
</script>
{{end}}

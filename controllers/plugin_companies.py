# -*- coding: utf-8 -*-
# try something like
@auth.requires_login()
def companies_overview():
    return dict(message="")

@auth.requires_login()
def contacts_overview():
    company_list = []

    companies = db(db.crm_companies).select(db.crm_companies.id,db.crm_companies.company_name,orderby=(db.crm_companies.company_name))

    for c in companies:
        company_list.append({'id':str(c.id),'text':c.company_name})

    return dict(company_list=company_list)

@auth.requires_login()
def all_companies():
    import json
    import plugin_sys
    json_details = ''

    companies_list = []

    companies = db(db.crm_companies).select(db.crm_companies.id,db.crm_companies.company_name,db.crm_companies.phone,db.crm_companies.address,db.crm_companies.province,db.crm_companies.city,db.crm_companies.postal,orderby=(db.crm_companies.company_name))

    users = db(db.auth_user).select(db.auth_user.id,db.auth_user.username)
    for c in companies:
        companies_list.append({'id':str(c.id),
                        'company_name':c.company_name,
                        'phone':c.phone,
                        'address':c.address,
                        'province':c.province,
                        'city':c.city,
                        'postal':c.postal
                         })

        json_details = json.dumps(companies_list)
    return response.json(json_details)

@auth.requires_login()
def all_contacts():
    import json
    import plugin_sys
    json_details = ''

    contacts_list = []
    company_list = []

    contacts = db(db.crm_contacts).select(db.crm_contacts.ALL,orderby=(db.crm_contacts.company_name))
    companies = db(db.crm_companies).select(db.crm_companies.id,db.crm_companies.company_name,orderby=(db.crm_companies.company_name))

    users = db(db.auth_user).select(db.auth_user.id,db.auth_user.username)
    for c in contacts:
        cName =plugin_sys.company_name_from_cid(companies,c.company_name)
        contacts_list.append({'id':str(c.id),
                        'company_id':str(c.company_name),
                        'company_name':cName,
                        'first_name':c.fname,
                        'last_name':c.lname,
                        'email':c.email,
                        'work_phone':c.work_phone,
                        'extension':c.extension,
                        'mobile_phone':c.mobile_phone
                         })

        json_details = json.dumps(contacts_list)
    return response.json(json_details)

@auth.requires_login()
def addCompany():
    import traceback
    name = request.vars.name
    phone = request.vars.phone
    fax = request.vars.fax
    address = request.vars.address
    city = request.vars.city
    state = request.vars.state
    czip = request.vars.czip

    try:
        db.crm_companies.insert(
            company_name=name,
            phone=phone,
            fax=fax,
            address=address,
            province=state,
            city=city,
            postal=czip
        )
        db.commit()
        state = True
    except:
        state = False

    return state

@auth.requires_login()
def editCompany():
    import traceback
    cid = request.vars.cid
    name = request.vars.name
    phone = request.vars.phone
    fax = request.vars.fax
    address = request.vars.address
    city = request.vars.city
    state = request.vars.state
    czip = request.vars.czip

    try:
        db(db.crm_companies.id == cid).update(
            company_name=name,
            phone=phone,
            fax=fax,
            address=address,
            province=state,
            city=city,
            postal=czip
        )
        db.commit()
        state = True
    except:
        state = traceback.format_exc()

    return state

@auth.requires_login()
def addContact():
    import traceback
    cname = request.vars.cname
    cfname = request.vars.cfname
    clname = request.vars.clname
    cemail = request.vars.cemail
    cwphone = request.vars.cwphone
    cext = request.vars.cext
    cmobile = request.vars.cmobile

    try:
        db.crm_contacts.insert(
            company_name=cname,
            fname=cfname,
            lname=clname,
            email=cemail,
            work_phone=cwphone,
            extension=cext,
            mobile_phone=cmobile
        )
        db.commit()
        state = True
    except:
        state = False

    return state

@auth.requires_login()
def editContact():
    import traceback
    cid = request.vars.cid
    cname = request.vars.cname
    cfname = request.vars.cfname
    clname = request.vars.clname
    cemail = request.vars.cemail
    cwphone = request.vars.cwphone
    cext = request.vars.cext
    cmobile = request.vars.cmobile

    try:
        db(db.crm_contacts.id == cid).update(
            company_name=cname,
            fname=cfname,
            lname=clname,
            email=cemail,
            work_phone=cwphone,
            extension=cext,
            mobile_phone=cmobile
        )
        db.commit()
        state = True
    except:
        state = traceback.format_exc()

    return state

# -*- coding: utf-8 -*-
# try something like
def index(): return dict(message="hello from plugin_crm.py")

def companies():
    companies = db().select(db.companies.ALL)
    return dict(companies=companies)

def add_companies():

    verticles = db().select(db.verticles.ALL)
    form = FORM(
        TABLE(
            TR('Company:',INPUT(_name='company_name',_style='width:250px')),
            TR('Phone:',INPUT(_name='phone',_style='width:250px')),
            TR('Website:',INPUT(_name='webaddress',_style='width:250px')),
            TR('Verticle(s):',SELECT(_name='verticles',*[OPTION(verticles[i].market,
                           _value=str(verticles[i].id)) 
                          for i in range(len(verticles))],_style='width:250px'),multiple=True),
            TR('Address',INPUT(_name='address',_style='width:250px')),
            TR('City',INPUT(_name='city',_style='width:250px')),
            TR('Postal Code',INPUT(_name='postal_code',_style='width:250px')),
            TR('State Province',INPUT(_name='state_province',_style='width:250px')),
            TR('Country',INPUT(_name='country',_style='width:250px')),
            TR('Phone',INPUT(_name='phone',_style='width:250px')),
            TR('Webaddress',INPUT(_name='webaddress',_style='width:250px')),
            TR('Primary Contact',INPUT(_name='primary_contact',_readonly="readonly",_style='width:250px')),
            TR('Title',INPUT(_name='title',_readonly="readonly",_style='width:250px')),
            TR('Email',INPUT(_name='email',_readonly="readonly",_style='width:250px')),
            TR('Verticles',INPUT(_name='verticle',_style='width:250px')),
            TR('Account Manager',INPUT(_name='account_manager',_style='width:250px')),
            TR('Account Status',INPUT(_name='account_status',_style='width:250px')),
            TR(TD(),TD(INPUT(_type='submit'))),_style='width:250px')
        )
    if form.process().accepted:
        company = form.vars.company_name
        phone = form.vars.phone
        url = form.vars.webaddress
        verticles = form.vars.vertilces
        try:
            company_info = db(db.companies.company_name==company).select(db.companies.ALL).first()
            if company_info:
                db(db.companies.id==company_info.id).update(company_name=company,
                                                          phone=phone,
                                                          webaddress=url,
                                                          verticle=verticles
                                                         )
                db.company_timeline.insert(company_id=int(company_info.id),timeline_event='Updated Profile',timeline_event_date=today)
            else:    
                x = db.companies.insert(company_name=company,
                                phone=phone,
                                webaddress=url,
                                verticle=verticles
                                )
                db.company_timeline.insert(company_id=int(x),timeline_event='Joined the Family',timeline_event_date=today)
            db.commit()
        except:
            pass
    return dict(form=form)

def contacts():
    form = SQLFORM(db.contacts)
    if form.accepts(request.vars, session):
        session.flash='done'
        redirect(URL(r=request))
    form = SQLFORM(db.company_info).process()
    companies = db(db.contacts).select(db.contacts.ALL)
    return locals()

def myaccounts():
    myaccounts = db(db.companies.account_manager==me).select(db.companies.ALL)
    for m in myaccounts:
        #if db((db.contacts.company==m.id)&(db.contacts.primary_contact==m.primary_contact)):
        primary_contact(m.id)
       
    if not myaccounts:
        myaccounts = "you don't have any accounts yet"
    return dict(myaccounts=myaccounts)

def prospecting():
    verticles = db().select(db.verticles.ALL)
    form = FORM(
        TABLE(
            TR('Company:',INPUT(_name='company_name',_style='width:250px')),
            TR('Phone:',INPUT(_name='phone',_style='width:250px')),
            TR('Website:',INPUT(_name='webaddress',_style='width:250px')),
            TR('Verticle(s):',SELECT(_name='verticles',*[OPTION(verticles[i].market,
                           _value=str(verticles[i].id)) 
                          for i in range(len(verticles))],_style='width:250px'),multiple=True),
            TR('Notes:',TEXTAREA(_name='notes'),colspan=3,_style='width:250px'),
            TR(TD(),TD(INPUT(_type='submit'))),_style='width:250px')
        )
    if form.process().accepted:
        company = form.vars.company_name
        phone = form.vars.phone
        url = form.vars.webaddress
        verticles = form.vars.vertilces
        if db(db.companies.company_name==company).select():
            db(db.companies.company_name==company).update(company_name=company,
                                                          phone=phone,
                                                          webaddress=url,
                                                          verticle=verticles
                                                         )
        else:    
            x = db.companies.insert(company_name=company,
                                phone=phone,
                                webaddress=url,
                                verticle=verticles
                                )
            db.company_timeline.insert(company_id=int(x),timeline_event='Prospected',timeline_event_date=today)
        db.commit()
    url = 'https://www.google.ca/?q=smartcone'
    return dict(url=url,form=form)

# -*- coding: utf-8 -*-
# try something like
def index():

    return dict(message='')

def getting_started():

    return dict(message='')
def compose():
    my_info = db(db.auth_user.id==me).select(db.auth_user.email).first()
    my_email = my_info.email
    
    return dict(my_email=my_email)

def createMessage():
    import base64
    from email.mime.text import MIMEText
    import json
    
    sender = request.vars.sender
    to = request.vars.to
    subject = request.vars.subject
    message_text = request.vars.message_text
    
    """
    Create a message for an email.

    Args:
    sender: Email address of the sender.
    to: Email address of the receiver.
    subject: The subject of the email message.
    message_text: The text of the email message.

    Returns:
    An object containing a base64url encoded email object.
    """
    message = MIMEText(message_text)
    message['to'] = to
    message['from'] = sender
    message['subject'] = subject
    
    message_raw = {'raw': base64.urlsafe_b64encode(message.as_string())}
    
    message_json = json.dumps(message_raw)
    
    return response.json(message_json)

def send_message(service, user_id, message):
    """
    Send an email message.

    Args:
    service: Authorized Gmail API service instance.
    user_id: User's email address. The special value "me"
    can be used to indicate the authenticated user.
    message: Message to be sent.

    Returns:
    Sent Message.
    """
    message = (service.users().messages().send(userId=user_id, body=message).execute())
    return message

# -*- coding: utf-8 -*-
# try something like
def index(): return dict(message="hello from plugin_sales.py")

@auth.requires_login()
def addOpportunity():
    from datetime import datetime,date,timedelta;
    import traceback
    cname = request.vars.cname
    odesc = request.vars.odesc
    oam = request.vars.oam
    ostage = request.vars.ostage
    ocd = request.vars.ocd
    ocd = datetime.strptime(ocd, '%Y-%m-%d').date()
    onote = request.vars.onote

    try:
        oid = db.opportunities.insert(
            company_id=cname,
            account_manager_id=oam,
            description=odesc,
            stage=ostage,
            target_close_date=ocd
        )
        db.opportunity_notes.insert(
            opportunity_id=oid,
            user_id=me,
            note=onote
        )
        db.opportunity_history.insert(
            o_id=oid,
            stage=ostage,
            date_changed=now,
            user_id=me
        )
        db.commit()
        state = True
    except:
        state = False

    return state

@auth.requires_login()
def editOpportunity():
    import traceback
    oid = request.vars.oid
    cname = request.vars.cname
    odesc = request.vars.odesc
    oam = request.vars.oam
    ostage = request.vars.ostage
    ocd = request.vars.ocd
    ocd = datetime.strptime(ocd, '%Y-%m-%d').date()
    onote = request.vars.onote
    
    opportunities = db(db.opportunities.id==oid).select(db.opportunities.stage).first()

    try:
        db(db.opportunities.id == oid).update(
            company_id=cname,
            account_manager_id=oam,
            description=odesc,
            stage=ostage,
            target_close_date=ocd
        )
        if opportunities.stage != ostage:
            db.opportunity_history.insert(
                o_id=oid,
                stage=ostage,
                date_changed=now,
                user_id=me
            )
        db.commit()
        state = True
    except:
        state = traceback.format_exc()

    return state

@auth.requires_login()
def updateOpportunityStage():
    oid = int(request.vars.oid)
    stage = request.vars.stage
    
    try:
        db(db.opportunities.id == oid).update(
            stage=stage
        )
        db.commit()
        message = True
    except:
        message = False
        
    return message

@auth.requires_login()
def getOppertunityNotes():
    import json
    import plugin_sys
    oid = int(request.vars.oid)
    
    notes= db(db.opportunity_notes.id == oid).select(db.opportunity_notes.ALL)
    users = db(db.auth_user).select(db.auth_user.id,db.auth_user.username)
    
    notesList = []
    
    for n in notes:
        uName = plugin_sys.user_name_from_id(users,n.user_id)
        notesList.append({
            'id':str(n.id),
            'opportunity_id':n.opportunity_id,
            'user_id':n.user_id,
            'username':uName,
            'note':n.note
         })

        json_details = json.dumps(notesList)
    
    return json_details

@auth.requires_login()
def all_opportunities():
    import json
    import plugin_sys
    
    json_details = ''

    opportunities_list = []

    opportunities = db(db.opportunities).select(db.opportunities.ALL,orderby=(db.opportunities.company_id))
    opportunity_notes = db(db.opportunity_notes).select(db.opportunity_notes.ALL)
    companies = db(db.crm_companies).select(db.crm_companies.id,db.crm_companies.company_name,orderby=(db.crm_companies.company_name))
    users = db(db.auth_user).select(db.auth_user.id,db.auth_user.username)
    for o in opportunities:
        cName = plugin_sys.company_name_from_cid(companies,o.company_id)
        amName = plugin_sys.user_name_from_id(users,o.account_manager_id)
        opportunities_list.append({
            'id':str(o.id),
            'company_id':o.company_id,
            'company_name':cName,
            'am_id':o.account_manager_id,
            'am_Name':amName,
            'desc':o.description,
            'stage':o.stage,
            'close_date':str(o.target_close_date)
         })

        json_details = json.dumps(opportunities_list)
    return response.json(json_details)

def sales_orders():
    sid = request.args(0)
    odid = request.args(1)

    ## data representations

    db.sales_order_details.product_id.represent = lambda id,r:db.products(r.sales_order_details.product_id).product
    db.sales_order_details.id.represent = lambda id,r:A(r.sales_order_details.id,_href=URL('sales_orders',args=(sid,r.sales_order_details.id)))


    if sid == None:
        sid = 0

    ### Sales Order From ###
    company_list=db().select(db.compaines.ALL)
    order_form = FORM(
        TABLE(
            TR(TD(SELECT(_name='customer',
                        *[OPTION(company_list[i].company_name,_value=str(company_list[i].id))  for i in range(len(company_list))])),TD(INPUT(_type='submit')),

              )
            ))
    if sid > 0:
        order_form.vars.customer = sid
    if order_form.process(formname='order_form',keepvalues=True).accepted:
        if sid > 0:
            x = db(db.sales_orders.id==sid).update(customer_id=order_form.vars.customer)
        else:
            x = db.sales_orders.insert(customer_id=order_form.vars.customer)
            sid = x.id

    ### Order Details Form ###
    btn_text = 'add'
    product_list = db(db.products).select(db.products.ALL)
    pl = []
    for p in product_list:
        pl.append(dict(product=p.product))
    od_form = FORM(
        TABLE(
            TR(
                TD(SELECT(_name='product',*[OPTION(product_list[i].product,_value=str(product_list[i].id)) for i in range(len(product_list))]),_placeholder='select product',_id='product_price'),
                TD(INPUT(_name='qty',_value='',_placeholder='QTY')),
                TD(INPUT(_name='price',_value=product_list[i].selling_price,_placeholder='price')),
                TD(INPUT(_name='discount',_value='0',_placeholder='discount')),
                TD(INPUT(_name='submit',_value=btn_text,_type='submit')))
        ))

    if odid:
        btn_text='update'
        od_form.vars.product=db.sales_order_details(odid).product_id
        od_form.vars.qty=db.sales_order_details(odid).qty
        od_form.vars.discount=db.sales_order_details(odid).discount
        od_form.vars.price=db.sales_order_details(odid).price
        if od_form.process(formname='od_form').accepted:
            db(db.sales_order_details.id==odid).update(sales_order_id=sid,product_id=od_form.vars.product,price=od_form.vars.price,qty=od_form.vars.qty)
            db.commit()
    else:
        if od_form.vars.price !=None:
            od_form.vars.price=db.products(od_form.vars.product).selling_price
        if od_form.process(formname='od_form').accepted:
            db.sales_order_details.insert(sales_order_id=sid,product_id=od_form.vars.product,price=od_form.vars.price,qty=od_form.vars.qty)
            db.commit()


    ### Order Details ###
    if sid:
        query = ((db.sales_order_details.sales_order_id==sid)&(db.products.id==db.sales_order_details.product_id))
        fields = (db.sales_order_details.id,db.products.product,db.products.description,db.sales_order_details.qty,db.sales_order_details.discount,db.sales_order_details.line_total)
        so_details = SQLFORM.grid(query=query,fields=fields,csv=False,user_signature=False,create=False,details=False,editable=False,searchable=False,maxtextlength=100)
        so_total = db(db.sales_orders.id==sid).select(db.sales_orders.ALL)
    else:
        so_details = 'Select a product to begin'
        so_total = ''

    return dict(order_form=order_form,od_form=od_form,so_details=so_details,so_total=so_total,pl=pl)

def opportunities_overview():
    company_list = []
    users_list = []
    stage_list = []

    companies = db(db.crm_companies).select(db.crm_companies.id,db.crm_companies.company_name,orderby=(db.crm_companies.company_name))
    users = db(db.auth_user).select(db.auth_user.id,db.auth_user.username)
    
    company_list.append({'id':'0','text':'Select a company'})
    users_list.append({'id':'0','text':'Select a User'})
    stage_list.append({'id':'0','text':'Select a stage'})
    
    for c in companies:
        company_list.append({'id':str(c.id),'text':c.company_name})
    for u in users:
        users_list.append({'id':str(u.id),'text':u.username})
    for s in STAGES:
        stage_list.append({'id':s,'text':s})

    return dict(company_list=company_list,users_list=users_list,stage_list=stage_list)

def opportunities():
    form = SQLFORM(db.sales)

    if form.accepts(request.vars, session):
        session.flash='New opportunity addeed'
        redirect(URL(r=request))
    return locals()

def trips():
    form = SQLFORM(db.trips)
    if form.process().accepted:
        session.flash='done'
        redirect(URL(r=request))
    trips = db().select(db.trips.ALL)
    return locals()

def expense_reports():
    form = SQLFORM(db.expense_reports).process()
    if form.process().accepted:
        redirect(URL(r=request))
    my_expense = SQLFORM.grid(db.expense_reports)
    return locals()

def company_info():
    form = SQLFORM(db.companies).process()
    if form.process().accepted:
        redirect(URL(r=request))
    companies = db(db.companies).select(db.companies.ALL)
    return locals()

def new_quote():
    contact_info = ''
    product_info = ''
    contact_list = ''
    cid = request.args(0)
    discounts = ['0','5','10','15','20','25']
    company_info = db(db.companies).select(db.companies.ALL).first()
    

    td = date.today()
    today = td.strftime("%B %d, %Y")

    company_form = FORM(
                            LABEL('Select a Company'),
                            DIV(
                                SELECT(_name='company_select',_class='form-control select2',*[OPTION(company_list[i].company_name,_value=str(company_list[i].id)) for i in range(len(company_list))]),
                            DIV(
                                INPUT(_type='submit',_class='btn btn-default bg-app btn-flat'),
                                _class='input-group-btn',
                                ),
                                _class='input-group',
                            )
                        )
    sold_to = 'False'

    if company_form.process(formname='company_form').accepted:
        cid = company_form.vars.company_select
        sold_to = db(db.companies.id==cid).select(db.companies.ALL).first()
        contact_list = db(db.contacts.company==cid).select(db.contacts.ALL)
        
        redirect(URL(r=request,args=cid))

    contact_form = FORM(
        TABLE(
            TR(
                TD(LABEL('contact'),SELECT(_name='contact',*[OPTION('%s %s'%(contact_list[i].first_name,contact_list[i].last_name),_value=str(contact_list[i].id)) for i in range(len(contact_list))]),_placeholder='select contact',_id='contact_price'),
                TD(INPUT(_name='submit',_value='Submit',_type='submit'))
            )
        )
    )
    if contact_form.process(formname='contact_form').accepted:
        contact_id=contact_form.vars.contact
        contact_info = db(db.contacts.id==contact_id).select(db.contacts.ALL).first()
        company_info = db(db.companies.id==company_list[i].id).select(db.companies.ALL).first()

    product_list = db(db.products).select(db.products.ALL)
    product_form = FORM(
                        DIV(
                            DIV(
                                LABEL('Quantity'),
                                INPUT(_placeholder='0',_class='form-control',_name='item_quantity'),
                                _class='col-xs-3'
                            ),
                            DIV(
                                LABEL('Product'),
                                SELECT(_name='product',_class='select2 form-control',_placeholder='select product',_id='product',onchange='jQuery(item_price).remove();',
                                  *[OPTION(product_list[i].product+' '+(product_list[i].description \
                                                                        if product_list[i].description!=None else '')+ \
                                           ' - $'+str(product_list[i].selling_price)
                                                           ,_value=str(product_list[i].id)) for i in range(len(product_list))]),
                                _class='col-xs-4'
                            ),
                            DIV(
                                LABEL('Discount'),
                                SELECT(OPTGROUP(*[OPTION(discounts[d]) for d in range(len(discounts))]),_name='discount',_class='select2 form-control'),
                                _class='col-xs-3'
                            ),
                            DIV(
                                INPUT(_type='submit',_value='Add Product',_class='btn btn-default btn-flat bg-app pull-right input-top'),
                                _class='col-xs-2'
                            ),
                            _class='row',
                        )
                    )

    selling_price = product_list[i].selling_price
    if product_form.process(formname='product_form').accepted:
        x = db.quotes.insert(company_id=cid)
        qid = x.id
        db.commit()
        this_product = db(db.products.id==product_form.vars.product).select(db.products.ALL).first()
        y = db.quote_details.insert(qty=product_form.vars.item_quantity,quote_id=qid,
                                    product_id=product_form.vars.product,discount=product_form.vars.discount,
                                    price=this_product.selling_price,line_total = ((float(product_form.vars.item_quantity)                                                                        *float(this_product.selling_price)*(1-(float(product_form.vars.discount)/100)))))


        db.commit()
        redirect(URL('plugin_sales','edit_quote',args=(cid,qid)))
        
        """contact_id=contact_form.vars.contact
        contact_info = db(db.contacts.id==contact_id).select(db.contacts.ALL).first()
        company_info = db(db.companies.id==contact_info.company).select(db.companies.ALL).first()"""

        
    quote_details = ''
    qt = '0.00'
    tax_rate = float(.13)
    tax = float(qt)*float(tax_rate)
    shipping=0
    total=(float(qt)+float(tax)+float(shipping))
    return dict(qt=qt,selling_price=selling_price,
                company_form=company_form,contact_form=contact_form,
                contact_info=contact_info,sold_to=sold_to,company_info=company_info,
                product_form=product_form,quote_details=quote_details,today=today,
                tax=tax,shipping=shipping,total=total
               )

def edit_quote():
    cid = request.args(0)
    qid = request.args(1)
    discounts = ['0','5','10','15','20','25']
    contact_info = ''
    product_info = ''
    contact_list = ''
    company_info = db(db.companies).select(db.companies.ALL).first()
    

    td = date.today()
    today = td.strftime("%B %d, %Y")
    account_manager = False
    company_form = db.companies(cid).company_name
    try:
        account_manager = db.auth_user(db.companies(db.quote(qid).id)).first_name
    except:
        account_manager = False
    
    sold_to = db(db.companies.id==cid).select(db.companies.ALL).first()
    ship = sold_to
    contact_list = db(db.contacts.company==cid).select(db.contacts.ALL)

    contact_form = FORM(
        TABLE(
            TR(
                TD(LABEL('contact'),SELECT(_name='contact',*[OPTION('%s %s'%(contact_list[i].first_name,contact_list[i].last_name),_value=str(contact_list[i].id)) for i in range(len(contact_list))]),_placeholder='select contact',_id='contact_price'),
                TD(INPUT(_name='submit',_value='Submit',_type='submit'))
            )
        )
    )
    if contact_form.process(formname='contact_form').accepted:
        contact_id=contact_form.vars.contact
        contact_info = db(db.contacts.id==contact_id).select(db.contacts.ALL).first()
        company_info = db(db.companies.id==company_list[i].id).select(db.companies.ALL).first()

    product_list = db(db.products).select(db.products.ALL)
    product_form = FORM(
                        DIV(
                            DIV(
                                LABEL('Quantity'),
                                INPUT(_placeholder='0',_class='form-control',_name='item_quantity'),
                                _class='col-xs-3'
                            ),
                            DIV(
                                LABEL('Product'),
                                SELECT(_name='product',_class='select2 form-control',_placeholder='select product',_id='product',onchange='jQuery(item_price).remove();',
                                  *[OPTION(product_list[i].product+' '+(product_list[i].description \
                                                                        if product_list[i].description!=None else '')+ \
                                           ' - $'+str(product_list[i].selling_price)
                                                           ,_value=str(product_list[i].id)) for i in range(len(product_list))]),
                                _class='col-xs-4'
                            ),
                            DIV(
                                LABEL('Discount'),
                                SELECT(OPTGROUP(*[OPTION(discounts[d]) for d in range(len(discounts))]),_name='discount',_class='select2 form-control'),
                                _class='col-xs-3'
                            ),
                            DIV(
                                INPUT(_type='submit',_value='Add Product',_class='btn btn-default btn-flat bg-app pull-right input-top'),
                                _class='col-xs-2'
                            ),
                            _class='row',
                        )
                    )

    selling_price = product_list[i].selling_price
    if product_form.process(formname='product_form').accepted:
        this_product = db(db.products.id==product_form.vars.product).select(db.products.ALL).first()
        if qid:
            new_qty = product_form.vars.item_quantity
            qd = db((db.quote_details.quote_id==qid)&(db.quote_details.product_id==product_form.vars.product)).select(db.quote_details.ALL)
            if len(qd)>0:
                for q in qd:
                    new_qty = int(q.qty) + int(product_form.vars.item_quantity)
                    db((db.quote_details.id==q.id)).update(qty=new_qty,
                                    discount=product_form.vars.discount,
                                    price=this_product.selling_price,line_total = ((float(new_qty) \
                                                                                   *float(this_product.selling_price)*(1-\
                                                                                    (float(product_form.vars.discount)/100)))))
            else:
                db.quote_details.insert(qty=product_form.vars.item_quantity,quote_id=qid,
                                    product_id=product_form.vars.product,discount=product_form.vars.discount,
                                    price=this_product.selling_price,line_total = ((float(product_form.vars.item_quantity) \
                                                                                   *float(this_product.selling_price)*(1-\
                                                                                    (float(product_form.vars.discount)/100)))))
                db.commit()
        else:
            x = db.quotes.insert()
            qid = x.id

            y = db.quote_details.insert(qty=product_form.vars.item_quantity,quote_id=qid,
                                    product_id=product_form.vars.product,discount=product_form.vars.discount,
                                    price=this_product.selling_price,line_total = ((float(product_form.vars.item_quantity) \
                                                                                   *float(this_product.selling_price)*(1-\
                                                                                    (float(product_form.vars.discount)/100)))))


        db.commit()
        redirect(URL(r=request,args=(cid,qid)))
        """contact_id=contact_form.vars.contact
        contact_info = db(db.contacts.id==contact_id).select(db.contacts.ALL).first()
        company_info = db(db.companies.id==contact_info.company).select(db.companies.ALL).first()"""

    query = ((db.quote_details.quote_id==qid)&(db.products.id==db.quote_details.product_id))
    quote_details = db(query).select(db.quote_details.ALL,db.products.ALL)
    try:
        rows = db(query).select(db.quote_details.line_total.sum())
        qt = rows.first()[db.quote_details.line_total.sum()]
        db(db.quotes.id==qid).update(quote_total=float(qt))
        db.commit()
    except:
        qt = '0.00'
    tax_rate = float(.13)
    tax = float(qt)*float(tax_rate)
    shipping=0
    total=(float(qt)+float(tax)+float(shipping))
    return dict(account_manager=account_manager,qt=qt,selling_price=selling_price,
                company_form=company_form,contact_form=contact_form,
                contact_info=contact_info,sold_to=sold_to,company_info=company_info,
                product_form=product_form,quote_details=quote_details,today=today,
                tax=tax,shipping=shipping,total=total,ship=ship
               )

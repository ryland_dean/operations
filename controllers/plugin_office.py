# -*- coding: utf-8 -*-
# try something like
def index(): return dict(message="hello from plugin_office.py")

@auth.requires_login()
def sheet():
    from openpyxl import Workbook
    import datetime
    import os
    wb = Workbook()
    n = request.args(0)
    if not n:
        n = '%s'%datetime.datetime.now()
    # grab the active worksheet
    ws = wb.active

    # Data can be assigned directly to cells
    c_list = db(db.ops_projects.company==db.crm_companies.id).select(db.ops_projects.ALL,db.crm_companies.ALL)
    rows = len(c_list)
    r_count = 0 # row counter based on the length of the client list
    c_count = 0 # column counter
    
    while True:
        if r_count == rows:
            break
        else:
            r_count = r_count + 1
        
        try:
            ws.cell(row=r_count,column=c_count+1,value = c_list[r_count].ops_projects.project_number)
            ws.cell(row=r_count,column=c_count+2,value = c_list[r_count].crm_companies.company_name)
            ws.cell(row=r_count,column=c_count+3,value = c_list[r_count].ops_projects.project_name)
            ws.cell(row=r_count,column=c_count+4,value = c_list[r_count].ops_projects.budget)
            ws.cell(row=r_count,column=c_count+5,value = c_list[r_count].ops_projects.actual)
            ws.cell(row=r_count,column=c_count+6,value = (c_list[r_count].ops_projects.budget-c_list[r_count].ops_projects.actual))
        except:
            break
    """
    # Rows can also be appended
    ws.append([1, 2, 3])
    """
    
    # Python types will automatically be converted
    """
    ws['A2'] = datetime.datetime.now()
    ws.cell(row=4,column=5,value=10)
    """
    directory = 'uploads/%s'%(db.auth_user(auth.user_id).username)
    
    if not os.path.exists(directory):
        os.mkdir(directory)
        
    # Save the file
    wb.save("%s/%s.xlsx"%(directory,n))
    fullpath = os.path.join("%s/%s.xlsx"%(directory,n))
    return response.stream(os.path.join(fullpath))

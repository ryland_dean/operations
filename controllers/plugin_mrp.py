# -*- coding: utf-8 -*-
# try something like
@auth.requires(True, requires_login=not request.is_local)
def index(): 
    return locals()

@auth.requires(True, requires_login=not request.is_local)
def suppliers():
    suppliers = db(db.suppliers).select(db.suppliers.ALL)
    return dict(suppliers=suppliers)

@auth.requires(True, requires_login=not request.is_local)
def add_supplier():
    form = SQLFORM(db.suppliers)
    if form.process().accepted:
        redirect(URL('plugin_mrp','suppliers'))
    return locals()

@auth.requires(True, requires_login=not request.is_local)
def edit_supplier():
    sid = request.args(0)
    form = crud.update(db.suppliers, sid)
    parts = db(db.parts_supply.supplier==sid).select(db.parts_supply.ALL)
    return locals()

@auth.requires(True, requires_login=not request.is_local)
def part_supplier():
    parts = db(db.parts.id==request.args(0)).select(db.parts.ALL)
    form = SQLFORM(db.parts_supply)
    form.vars.part = request.args(0)
    if form.process().accepted:
        redirect(URL(r=request,f='all_parts'))
    return locals()

@auth.requires(True, requires_login=not request.is_local)
def parts_list():
    db.parts.id.represent = lambda id, r: A(r.part,_href=URL('plugin_mrp','view_part', args=id))
    parts = SQLFORM.grid(db.parts, csv=False, orderby=db.parts.part, links_in_grid=True)
    return locals()

@auth.requires(True, requires_login=not request.is_local)
def all_parts():
    form = SQLFORM(db.parts).process()
    parts = db().select(db.parts.ALL)
    return dict(form=form,parts=parts)

@auth.requires(True, requires_login=not request.is_local)
def edit_part():
    form = SQLFORM(db.parts, request.args(0))
    part_id = db(db.parts.id==request.args(0)).select(db.parts.ALL)
    if form.process().accepted:
        redirect(URL(r=request,f='all_parts'))
    return locals()

@auth.requires(True, requires_login=not request.is_local)
def add_part():
    form = SQLFORM(db.parts)
    if  form.process().accepted:
        redirect(URL(r=request))
    all_parts = db().select(db.parts.ALL)
    return dict(form=form, all_parts=all_parts)

@auth.requires(True, requires_login=not request.is_local)
def view_part():
    part = db(db.parts.id==request.args(0)).select(db.parts.ALL)
    suppliers = db(db.parts.part==request.args(1)).select(db.parts.ALL)
    return locals()

@auth.requires(True, requires_login=not request.is_local)
def subassemblies():
    form = SQLFORM(db.subassemblies).process()
    subs = db().select(db.subassemblies.ALL)
    return locals()

@auth.requires(True, requires_login=not request.is_local)
def subassembly_parts_list():
    parts = db((db.parts.id==db.subassembly_parts_list.part)&(db.subassembly_parts_list.subassembly==request.args(0))).select(db.parts.ALL)
    
    form = crud.create(db.subassembly_parts_list)
    form.vars.subassembly = db.subassemblies(request.args(0)).id
    if form.accepts(session,request):
        redirect(URL(r=request,args=(request.args(0))))
    
    return locals()

@auth.requires(True, requires_login=not request.is_local)
def assemblies():
    form = SQLFORM(db.assemblies).process()
    assemblies = db().select(db.assemblies.ALL)
    return locals()

@auth.requires(True, requires_login=not request.is_local)
def assembly_parts_list():
    assemblies = db(db.assemblies.id==request.args(0)).select()
    parts = db(db.parts.assembly==assemblies[0].id\
                  ).select()
    form = SQLFORM(db.assembly_parts_list)
    form.vars.assembly = request.args(0)
    if form.process().accepted:
        redirect(URL(r=request,args=(request.args(0))))
    return dict(assemblies=assemblies,form=form,parts=parts)

@auth.requires(True, requires_login=not request.is_local)
def boms():
    form = SQLFORM(db.boms).process()
    
    assembly_list = db(db.assemblies).select(db.assemblies.ALL)
    alist = []
    for a in assembly_list:
        alist.append(a.id)
    
    boms_form = FORM(
                    DIV(
                        LABEL('Product BOM'),
                        INPUT(_type='text',_name='product_bom',_class='form-control')
                    ,_class='form-group'),
                    DIV(
                        LABEL('Description'),
                        INPUT(_type='text',_name='description',_class='form-control')
                    ,_class='form-group'),
                    DIV(
                        LABEL('Assembly'),
                        SELECT(_name='assembly',_class='form-control select2',*[OPTION(assembly_list[i].assembly,_value=str(assembly_list[i].id),_multiple=True) for i in range(len(assembly_list))])
                    ,_class='form-group'),
                    DIV(
                        LABEL('F Cost'),
                        INPUT(_type='text',_name='f_cost',_class='form-control',_placeholder='0.00')
                    ,_class='form-group'),
                    DIV(
                        INPUT(_type='submit',_class='btn btn-default btn-flat bg-app')
                    ,_class='form-group')
                )
    
    boms = db().select(db.boms.ALL)
    return locals()

def receiving():
    return locals()

def shipping():
    return locals()

@auth.requires(True, requires_login=not request.is_local)
def add_category():
    form = crud.create(db.categories)
    return locals()

@auth.requires(True, requires_login=not request.is_local)
def manage_categories():
    cats = db(db.categories).select(db.categories.ALL)
    if len(cats) == 0:
        redirect(URL('add_category'))
    cid = request.args(0)
    if cid > 0:
        form = crud.update(db.categories,cid)
    else:
        form = crud.select(db.categories)
        
    return locals()

# Live Views Version 1.1.2

# -*- coding: utf-8 -*-
# try something like
me = auth.user_id
import traceback
import plugin_sys
import plugin_livestreams

def fav_monitor():
    vars = request.post_vars
    nid = vars.nid
    mid = vars.mid
    mname = vars.monName
    feed = vars.feed
    proxy = vars.proxy
    user = vars.user
    thmb = vars.thmb
    wtype = 1
    
    db.widgets.insert(
        widget_type=wtype,
        node_id=nid,
        monitor_id=mid,
        monitor_name=mname,
        user_id=user,
        proxy_prefix=proxy,
        url_feed=feed,
        static_content=thmb
    )
    db.commit()
    
    return('Camera has been added to your dashboard!')

def unfav_monitor():
    vars = request.post_vars
    nid = vars.nid
    mid = vars.mid
    user = vars.user
    
    db((db.widgets.node_id==nid)&(db.widgets.monitor_id==mid)&(db.widgets.user_id==user)).delete()
    
    db.commit()
    
    return('Camera has been removed to your dashboard!')

def report_camera():
    vars = request.post_vars
    nid = vars.nid
    nname = vars.nodeName
    mid = vars.mid
    mname = vars.monName
    rdesc = vars.rdesc
    thmb = vars.thmb
    
    mail.send('support@thesmartcone.com',
        'Camera Issue Report from %s'%(request.env.http_host),
        '<html><h2>Issue with %s(%s) on %s(%s)</h2><p>%s</p></html>'%(mname,mid,nname,nid,rdesc),
        attachments = mail.Attachment('%s'%(thmb), content_id='monitor_thumbnail'))
    
    return('Camera issue report has been sent!')

def profile_live_view():
    import plugin_sys
    pName = request.vars.pName
    if pName == None:
        redirect(URL(c='plugin_livestreams',f='posters'))
    
    favd = []
    node_name = []
    
    widgets = db(db.widgets.user_id == me).select(db.widgets.node_id,db.widgets.monitor_id)
    
    monitors = db(db.view_profiles.profile_name==pName).select(db.view_profiles.ALL,orderby=db.view_profiles.monitor_order)
    
    for m in monitors:
        favd.append(plugin_livestreams.fav_check(widgets,m.node_id,m.monitor_id))
        node_name.append(plugin_sys.node_name_from_id(servers,m.node_id))
        
    return dict(monitors=monitors,pName=pName,me=me,favd=favd,widgets=widgets,node_name=node_name)

def livestream():
    sid = request.vars.nid
    if sid==None:
        sid=1
    server_id = sid
    
    monitors_list = []
    
    monitors = []
    streams = []
    fav_feed = []
    mons = []
    mids = []
    favd = []
    thumbnail_paths = []
    poster_paths = []
    node_list = db(db.nodes.node_type==1).select(db.nodes.ALL)

    mon_search = FORM(
        DIV(
            SELECT(_name='select_node',_class='select2 form-control node-select',*[OPTION(node_list[i].detailed_name,_value=node_list[i].id) for i in range(len(node_list))])
        ,_class="form-group")
    ,_class="form")
    
    widgets = db(db.widgets.user_id == me).select(db.widgets.node_id,db.widgets.monitor_id)
    
    users = db(db.auth_user).select(db.auth_user.username,db.auth_user.id)
    ims = db(db.nodes.id==sid).select(db.nodes.ALL).first()
    try:
        server_name = ims.detailed_name
        proxy_pass = ims.proxy_prefix
        dbzm = DAL('mysql://%s:%s@%s/zm'%(ims.zm_user,ims.zm_pass,ims.ip_address),lazy_tables=True,pool_size=10,migrate=False)
        no_connection = False
        dbzm.define_table('Monitors',
                        Field('Id','id'),
                        Field('Type'),
                        Field('Name'),
                        Field('Function'),
                        Field('Path'),
                        Field('Height'),
                        Field('Width'),
                      Field('SectionLength')
              )

        monitors = dbzm(dbzm.Monitors.Function!='None').select(dbzm.Monitors.ALL)
        for m in monitors:
            streams.append('%s://%s/%s/zm/cgi-bin/nph-zms?monitor=%d&scale=25&maxfps=1&buffer=1'%(request.env.request_scheme,request.env.http_host,ims.proxy_prefix,m.Id))
            fav_feed.append('%s://%s/%s/zm/cgi-bin/nph-zms?monitor=%d&scale=40&maxfps=1&buffer=1'%(request.env.request_scheme,request.env.http_host,ims.proxy_prefix,m.Id))
            #streams.append('/cgi-bin/nph-zms?monitor=%d&scale=25&maxfps=1&buffer=1'%(m.Id))
            mons.append(m.Name)
            mids.append(m.Id)
            favd.append(plugin_livestreams.fav_check(widgets,sid,m.Id))
        
        dbmons = DAL('mysql://%s:%s@%s/%s'%(ims.videos_user,ims.videos_pass,ims.ip_address,ims.videos_database),lazy_tables=True,pool_size=10,migrate=False)
        
        dbmons.define_table('monitors',
                Field('MonitorId','integer'),
                Field('Monitor_Name'),
                Field('camera_type'),
                Field('Function'),
                Field('monitor_thumbnail'),
                Field('monitor_poster')
        )
        
        monitors_list = dbmons((dbmons.monitors.Function != 'Disconnected')|
                          (dbmons.monitors.Function==None)).select(dbmons.monitors.monitor_thumbnail,dbmons.monitors.monitor_poster)
        
        for mt in monitors_list:
            thumbnail_paths.append(str(mt.monitor_thumbnail))
            poster_paths.append(str(mt.monitor_poster))
        
    except:
        mail.send('support@thesmartcone.com',
                  'Error Message on %s(%s) from %s in %s'%(ims.detailed_name,ims.ip_address,plugin_sys.user_name_from_id(users,auth.user_id),ims.node_url),
                  '<html>%s %s/admin</html>'%(traceback.format_exc(),'https://%s'%(request.env.http_host)))
        
    return dict(streams=streams,mons=mons,sid=sid,server_name=server_name,server_id=server_id,mids=mids,thumbnail_paths=thumbnail_paths,poster_paths=poster_paths,proxy_pass=proxy_pass,me=me,favd=favd,widgets=widgets,fav_feed=fav_feed)

def submit_profile():
    
    vars = request.post_vars
    nid = vars.nid
    monName = vars.monName
    mid = vars.mid
    uid = vars.uid
    proxy = vars.proxy
    feed = vars.feed
    thmb = vars.thmb
    order = vars.order
    pName = vars.pName
    
    db.view_profiles.insert(
        profile_name=pName,
        node_id=nid,
        monitor_name=monName,
        monitor_id=mid,
        user_id=uid,
        proxy_prefix=proxy,
        monitor_feed=feed,
        monitor_thumbnail=thmb,
        monitor_order=order
    )
    db.commit()
    
    return("Success")

def posters():
    
    connection = ""
    err_msg = "None"
    monitors = []
    monitors_list = []
    zm_monitors = []
    
    nodes = db(db.nodes.capabilities.contains('video_library')).select(db.nodes.id,db.nodes.proxy_prefix,db.nodes.detailed_name,db.nodes.node_name,db.nodes.videos_user,db.nodes.videos_pass,db.nodes.zm_user,db.nodes.zm_pass,db.nodes.ip_address,db.nodes.videos_database)
    profile_names = db(db.view_profiles).select(db.view_profiles.profile_name)
    
    troubled_nodes = []
    
    for n in nodes:
        try:
            dbmons = DAL('mysql://%s:%s@%s/%s'%(n.videos_user,n.videos_pass,n.ip_address,n.videos_database),lazy_tables=True,pool_size=10,migrate=False)
            dbmons.define_table('monitors',
                Field('MonitorId','integer'),
                Field('Monitor_Name'),
                Field('NodeId'),
                Field('camera_type'),
                Field('Function'),
                Field('monitor_thumbnail'),
                Field('monitor_poster')
            )

            class monitor:
                def __init__(self):
                    self.id = None
                    self.name = None
                    self.nodeId = None
                    self.thumbnail = None
                    self.poster = None
            
            monitors = dbmons((dbmons.monitors.Function != 'Disconnected')|
                          (dbmons.monitors.Function==None)).select(dbmons.monitors.ALL)
            
            for m in monitors:
                x = monitor()
                x.id = m.MonitorId
                x.name = m.Monitor_Name
                x.nodeId = m.NodeId
                x.thumbnail = m.monitor_thumbnail
                x.poster = m.monitor_poster
                monitors_list.append(x)

            connection = "True"

        except:
            troubled_nodes.append(n.ip_address)
            continue
            #connection = "False"
            #err_msg = traceback.format_exc()

    return dict(nodes=nodes,connection=connection,monitors_list=monitors_list,me=me,profile_names=profile_names,err_msg=err_msg,troubled_nodes=troubled_nodes)

def getMons():
    vars = request.post_vars
    nid = vars.nid
    return nid

def stream_thumbnail():
    import os
    fullpath = request.vars.fp
    return response.stream(open(fullpath),chunk_size=10**6)

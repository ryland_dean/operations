# -*- coding: utf-8 -*-
# try something like
def index(): return dict(message="hello from plugin_admin.py")

def users():
    users = db(db.auth_user).select(db.auth_user.ALL)
    groups = db(db.auth_group).select(db.auth_group.ALL)
    members = db((db.auth_membership.user_id==db.auth_user.id)&(db.auth_group.id==db.auth_membership.group_id)).select(db.auth_membership.ALL,db.auth_group.ALL)
    
    return dict(users=users,groups=groups,members=members)

def profile():
    
    uid = request.args(0)
    if not uid:
        uid = me

    profile = db(db.auth_user.id==uid).select(db.auth_user.ALL)
    groups = db(db.auth_group).select(db.auth_group.ALL)
    members = db((db.auth_membership.user_id==db.auth_user.id)&(db.auth_group.id==db.auth_membership.group_id)).select(db.auth_membership.ALL,db.auth_group.ALL)

    return dict(profile=profile,groups=groups,members=members)

# -*- coding: utf-8 -*-
# try something like
def index(): return dict(message="hello from plugin_sims.py")

def add_sim():
    qs = (db.sims)
    form = SQLFORM(qs)
    if form.process().accepted:
        redirect(URL(r=request))
    sims = db(db.sims.supplier==db.sim_suppliers.id).select(db.sims.ALL,db.sim_suppliers.ALL)
    return dict(form=form,sims=sims)

def sims():
    
    sims = db(db.sims.supplier==db.sim_suppliers.id).select(db.sims.ALL,db.sim_suppliers.ALL)
    
    form = SQLFORM(db.sims)
    
    return dict(sims=sims,form=form)

def activate_sim():
    rs = db(db.sims).select(db.sims.ALL)
    rsl = []
    for r in rs:
        rsl.append(r.id)
        
    row = (db.active_sims.sim_id!=db.sims.id) # only select sims that have not been activated.
    
    qs = db((row)&(db.sim_suppliers.id==db.sims.supplier)&(db.sim_suppliers.id==db.sim_data_plans.sim_supplier)).select(db.sims.ALL,db.sim_suppliers.ALL,db.sim_data_plans.ALL)
    qc = db(db.crm_companies).select(db.crm_companies.ALL)
    
    form = FORM(TABLE(TR('Select a SIM Card',(SELECT(_name='Sims',*[OPTION((qs[i].sims.sim_number +' - '+ qs[i].sim_suppliers.company_name +' - '+ qs[i].sim_data_plans.plan_type),
                                                      _value=str(qs[i].sims.id)) for i in range(len(qs))]))),
                      
                      TR('Select a cusotmer',SELECT(_name='Customer',*[OPTION(qc[i].company_name,
                                                      _value=str(qc[i].id)) for i in range(len(qc))])),
                      TR('Select the activation date',INPUT(_type='date')),
                      
                      TR('Choose the billing cycle',SELECT(_name='billing cycle',*[OPTION('Montly'),OPTION('Annually'),OPTION('Fixed')])),
                      
                      TR(INPUT(_value='Activate',_type='Submit'))))
    
    if form.process().accepted:
        db.active_sims.insert(sim_id=form.vars.sims,customer_id=form.vars.company_id,data_plan=qs[i].sim_data_plans.plan_type,startdate=form.vars.date,billing_cycle='monthly')
        redirect(URL(r=request))
    
    return dict(form=form)

def active_sims():
    qs = (db.active_sims)
    qc = (db.crm_companies)
    qd = (db.sim_data_plans)

    sims = db((db.active_sims.customer_id==db.crm_companies.id)&
              (db.sims.id==db.active_sims.sim_id)&
              (db.sim_data_plans.id==db.active_sims.data_plan)).select(db.sims.ALL,
                                                                    db.active_sims.ALL,
                                                                    db.crm_companies.ALL,
                                                                    db.sim_data_plans.ALL)
    
    form = SQLFORM(db.active_sims)
    
    return dict(sims=sims,form=form)

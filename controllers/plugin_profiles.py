# -*- coding: utf-8 -*-
# try something like
def index(): return dict(message="hello from plugin_profiles.py")

def login_status():
    if auth.user:
        return 'true'
    else:
        return 'false'

def users_overview():
    
    return dict(message="")

def user_work_analytics():
    import json
    weekly_hours = 37.5
    
    users = db(db.auth_user).select(db.auth_user.id,db.auth_user.username)
    timecards = db((db.ops_time_card.work_done_on>=last_week)&(db.ops_time_card.work_done_on<=today)).select(db.ops_time_card.user_id,db.ops_time_card.work_done_on,db.ops_time_card.hours)
    tasks = db((db.ops_tasks.due_date<=week)&(db.ops_tasks.due_date>=today)).select(db.ops_tasks.user_id,db.ops_tasks.time_estimate,db.ops_tasks.due_date)
    
    user_list = []
    for u in users:
        timecards_list = []
        time_estimate_list = []
        for tc in timecards:
            if tc.user_id == u.id:
                timecards_list.append(tc.hours)
        total_worked_time = sum(timecards_list)
        worked_percentage = (float(total_worked_time)/float(weekly_hours))*100.0
        for t in tasks:
            if t.user_id == u.id:
                if None == t.time_estimate:
                    te = 1
                else:
                    te = t.time_estimate
                time_estimate_list.append(te)
        total_time_estimate = sum(time_estimate_list)
        availabilty = (float(total_time_estimate)/float(weekly_hours))*100.0
        user_list.append({
                'user_id':str(u.id),
                'username':u.username,
                'wp':worked_percentage,
                'av':availabilty
            })

    json_details = json.dumps(user_list)

    return json_details

def profile_picture():
    users = db(db.auth_user).select(db.auth_user.id,db.auth_user.username)
    userdict = {x.id:x.username for x in users}
    user_profiles = db(db.user_profile).select(db.user_profile.ALL)

    import os
    import subprocess
    from datetime import datetime
    me = auth.user_id
    now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    data = ''
    message = 'OK'
    #try:
    photo_upload = FORM(
        DIV(
            LABEL("Filename:",_for="name"),
            INPUT(_name="name",_type="text",_class="form-control"),_class="form-group"),
        DIV(
            INPUT(_name='file',_type='file'),
        _class="form-group"),
        INPUT(_type='submit',_class="btn btn-default btn flat bg-red"),
    _role="form")

    if photo_upload.process(formname='photo_upload').accepted:
        #new_path = '/usr/share/www-data/web2py/applications/%s/uploads/profile_photos/'%(request.application)
        new_path = os.path.join(request.folder,'/uploads/profile_photos')
        filename = photo_upload.vars.name
        filename = filename.replace(' ','_')
        filename = filename.replace('(','')
        filename = filename.replace(')','')
        filename = filename[:-4] + '.jpg'
        new_file = os.path.join(new_path,filename)
        
        #if not os.path.isfile(new_file):
        if not os.path.exists(new_path):
            os.makedirs(new_path)

        data = photo_upload.vars["file"].value
        if not os.path.isfile(new_file):
            with open(new_file, "wb") as f:
                f.write(data)

            db.user_profile.insert(
                    user_id=me,
                    name=photo_upload.vars.name,
                    image=filename,
                    image_path=new_path,
                    uploaded_by=me,
                    uploaded_on=now)
            db.commit()
        else:
            message = 'File already exists!'
       
    return dict(form=photo_upload,data=data,me=me,message=message,userdict=userdict)

def stream_profile_photo():
    import os
    uid = int(request.args(0))
    #fullpath = request.vars.fp
    #image = db.user_profile(uid).image
    profile_image = db(db.user_profile.user_id==uid).select(db.user_profile.image).first()
    if profile_image:
        image_name = profile_image.image
    else:
        image_name = 'default.jpg'
    
    image = os.path.join(request.folder+'/uploads/profile_photos/%s')%image_name

    return response.stream(open(image),chunk_size=10**6)

#!/usr/bin/env python
# -*- coding: utf-8 -*-
from gluon import *

def fav_check(favd,nid,mid):
    
    state = ""
    
    for f in favd:
        if (str(f.node_id) == str(nid) and str(f.monitor_id) == str(mid)):
            state = "true"
            break
            
    return(state)

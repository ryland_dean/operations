#!/usr/bin/env python
# -*- coding: utf-8 -*-
from gluon import *

def task_name_from_id(tList,tID):
    for t in tList:
        if t.id == tID:
            return t.task_name
    return ""

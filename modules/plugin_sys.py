#!/usr/bin/env python
# -*- coding: utf-8 -*-
from gluon import *

def user_name_from_id(uList,uID):
    for u in uList:
        if u.id == uID:
            return u.username
    return ""

def project_name_from_id(pList,pID):
    for p in pList:
        if p.id == pID:
            return p.project_name
    return ""

def company_name_from_cid(cList,cID):
    for c in cList:
        if c.id == cID:
            return c.company_name
    return ""

def company_name_from_pid(cList,pList,pcID):
    for c,p in zip(cList,pList):
        if c.id == p.company:
            return c.id
    return ""

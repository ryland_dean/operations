from datetime import datetime, timedelta,date
import time
now = datetime.today()
today = date.today()
four_weeks = today + timedelta(weeks=4)
me = auth.user_id

CURRENCY = ['CAN','USD']

PART_STATUS = ['In Development','BETA','Released']

LOCATIONS = ['Canada', 'USA', 'CHINA', 'INDIA', 'South Korea','OTHER']

LEADTIMES = ['3 days','1 week', '2 weeks', '3 weeks', '3 weeks +' ]



db.define_table('suppliers',
                   Field('supplier'),
                   Field('contact'),
                   Field('email',requires=IS_EMAIL()),
                   Field('address'),
                   Field('city'),
                   Field('postal_code'),
                   Field('state_province'),
                   Field('located_in', requires=IS_IN_SET(LOCATIONS),default='Canada'),
                   Field('website',requires=IS_URL()),
                   Field('phone'),
                   format='%(supplier)s')

db.define_table('categories',
                   Field('category'),
                   Field('description'),
                   format='%(category)s')

db.define_table('parts',
                   Field('part'),
                   Field('description'),
                   Field('category',db.categories),
                   Field('part_status',requires=IS_IN_SET(PART_STATUS), default='In Development'),
                   auth.signature,
                   format='%(part)s')

db.define_table('parts_supply',
                   Field('supplier','reference suppliers',default='ITAXIA CORP'),
                   Field('part','reference parts'),
                   Field('supplier_partnumber','string'),
                   Field('part_cost','decimal(15,2)',default=0.00,represent = lambda value,row: XML(DIV('$ %.2f' % (0.0 if value == None else value),_style='text-align: right;'))),
                   Field('currency',requires=IS_IN_SET(CURRENCY),default='CAN'),
                   Field('leadtime', requires=IS_IN_SET(LEADTIMES),default='3 days'),

                   auth.signature,
                   format='%(part)s')

db.define_table('subassemblies',
                   Field('subassembly', label='Subassembly'),
                   Field('f_cost','decimal(15,2)',default=0.00,writable=False,represent = lambda value,row: XML(DIV('$ %.2f' % (0.0 if value == None else value),_style='text-align: right;'))),
                   format='%(subassembly)s')

db.define_table('subassembly_parts_list',
                   Field('subassembly','reference subassemblies'),
                   Field('part','reference parts',length=150,unique=True),
                   Field('QTY',default=1))

db.define_table('assemblies',
                   Field('assembly'),
                   Field('f_cost','decimal(15,2)',default=0.00,writable=False,represent = lambda value,row: XML(DIV('$ %.2f' % (0.0 if value == None else value),_style='text-align: right;'))),
                   format='%(assembly)s')

db.define_table('assembly_parts_list',
                   Field('assembly','reference assemblies'),
                   Field('subassembly',db.subassemblies),
                   Field('part',requires=IS_NULL_OR(IS_IN_DB(db,'parts.id','%(part)s'))),
                   Field('QTY',default=1))

db.define_table('boms',
                   Field('product_bom'),
                   Field('description'),
                   Field('assembly','reference assemblies'),
                   Field('f_cost','decimal(15,2)',default=0.00,represent = lambda value,row: XML(DIV('$ %.2f' % (0.0 if value == None else value),_style='text-align: right;'))),
                   )

db.define_table('products',
                   Field('product'),
                   Field('description'),
                   Field('product_cost', 'decimal(15,2)', default=0.00,represent = lambda value,row: XML(DIV('$ %.2f' % (0.0 if value == None else value),_style='text-align: right;'))),
                   Field('selling_price','decimal(15,2)', default=0.00,represent = lambda value,row: XML(DIV('$ %.2f' % (0.0 if value == None else value),_style='text-align: right;'))))

db.define_table('inventory',
                Field('part','reference parts'),
                Field('QTY_on_hand','integer',default=0),
                Field('inventory_date','datetime',default=now),
                auth.signature,
                format='%(part)s')

db.define_table('inventory_items',
                Field('product','reference products'),
                Field('serial_number'),
                Field('mac_address'),
                Field('IMEI'),
                Field('MEID'),
                Field('supplier','reference suppliers')
               )

db.define_table('sales_orders',
                Field('customer_id','integer'),
                Field('order_date','date',default=today),
                Field('entered_by','integer',default=me),
                Field('delivery_dated','date',default=four_weeks),
                Field('subtotal','decimal(15,2)',default=0.00,represent = lambda value,row: XML(DIV('$ %.2f' % (0.0 if value == None else value),_style='text-align: right;'))),
                Field('tax_rate','decimal(3,1)',default=0.00),
                Field('taxes','decimal(15,2)',default=0.00,represent = lambda value,row: XML(DIV('$ %.2f' % (0.0 if value == None else value),_style='text-align: right;'))),
                Field('ship_hand','decimal(15,2)',default=0.00,represent = lambda value,row: XML(DIV('$ %.2f' % (0.0 if value == None else value),_style='text-align: right;'))),
                Field('so_total','decimal(15,2)',default=0.00,represent = lambda value,row: XML(DIV('$ %.2f' % (0.0 if value == None else value),_style='text-align: right;')))
               )

db.sales_orders.entered_by.represent=lambda entered_by,r:db.auth_user(r.entered_by).username

db.define_table('sales_order_details',
                Field('sales_order_id','integer'),
                Field('product_id','integer'),
                Field('qty','integer'),
                Field('price','decimal(15,2)',default=0.00,represent = lambda value,row: XML(DIV('$ %.2f' % (0.0 if value == None else value),_style='text-align: right;'))),
                Field('discount','decimal(3,1)',default=0.00,represent = lambda value,row: XML(DIV('%.2f' % (0.0 if value == None else value),_style='text-align: right;'))),
                Field('line_total','decimal(15,2)',default=0.00,represent = lambda value,row: XML(DIV('$ %.2f' % (0.0 if value == None else value),_style='text-align: right;')))
                )

db.define_table('purchase_orders',
                Field('date_added','datetime',default=now),
                Field('ordered_by','integer',default=me),
                Field('supplier_id','integer')
               )

db.define_table('purchase_order_details',
                Field('purchase_order_id','integer'),
                Field('part_id','integer'),
                Field('qty','integer'),
                Field('price','decimal(15,2)',default=0.00,represent = lambda value,row: XML(DIV('$ %.2f' % (0.0 if value == None else value),_style='text-align: right;'))),
                Field('line_total','decimal(15,2)',default=0.00,represent = lambda value,row: XML(DIV('$ %.2f' % (0.0 if value == None else value),_style='text-align: right;')))
                )

db.define_table('receiving',
                   Field('part','reference parts'),
                   Field('purchase_order_id','integer'),
                   Field('date_received','date'),
                   auth.signature)

db.define_table('shipping',
                Field('shipping_date','date'),
                Field('product','reference products'),
                Field('customer','integer'),
                auth.signature)


## calucate line total in sales orders

row = db(db.sales_order_details).select()
for r in row:
    if r.discount == None:
        r.discount = 0
    if r.qty == None:
        r.qty = 0
    if r.price == None:
        r.price=0
    db(db.sales_order_details.id==r.id).update(line_total=((r.qty*r.price)*(1-(r.discount/100))))
    db.commit()
        
#calculate sales order totals
so = db(db.sales_orders).select()
for s in so:
    row = db(db.sales_order_details.sales_order_id==s.id).select(db.sales_order_details.line_total.sum())
    nt = row.first()[db.sales_order_details.line_total.sum()]
    if nt == None:
        nt=0
    if s.tax_rate == None:
        s.tax_rate = 0
    t = nt*(s.tax_rate/100)
    if s.ship_hand == None:
        s.ship_hand = (t*nt)
    db(db.sales_orders.id==s.id).update(tax_rate=s.tax_rate,ship_hand=s.ship_hand,subtotal=nt,taxes=t,so_total=(nt+t+s.ship_hand))
    db.commit()

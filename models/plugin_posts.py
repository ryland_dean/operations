# -*- coding: utf-8 -*-
from datetime import datetime,date
from gluon.tools import prettydate
me = auth.user_id

db.define_table('ops_posts',
                Field('project_id','integer'),
                Field('task_id','integer'),
                Field('posted_by','integer',default=me),
                Field('post','text'),
                Field('tags','list:string'),
                auth.signature
               )

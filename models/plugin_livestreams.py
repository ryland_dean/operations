# -*- coding: utf-8 -*-
me = auth.user_id

db.define_table('view_profiles',
                    Field('profile_name'),
                    Field('node_id','integer'),
                    Field('monitor_id','integer'),
                    Field('monitor_name'),
                    Field('user_id','integer'),
                    Field('proxy_prefix'),
                    Field('monitor_feed'),
                    Field('monitor_thumbnail'),
                    Field('monitor_order')
                )

pNames = db(db.view_profiles.user_id==me).select(db.view_profiles.profile_name, groupby=db.view_profiles.profile_name)

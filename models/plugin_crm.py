# -*- coding: utf-8 -*-

me = auth.user_id

db.define_table('verticles',
                Field('market'),
                format='%(market)s'
               )


db.define_table('company_timeline',
                Field('company_id'),
                Field('timeline_event'),
                Field('timeline_event_date'),
                auth.signature
               )

db.define_table('contacts',
                Field('first_name'),
                Field('last_name'),
                Field('title'),
                Field('company'),
                Field('phone'),
                Field('ext'),
                Field('mobile'),
                Field('email'),
                Field('primary_contact','integer',default=0),
                auth.signature,
                format='%(first_name last_name)s')

db.define_table('notes',
                Field('company_id'),
                Field('author'),
                Field('note','text'),
                auth.signature
               )

def primary_contact(cid):
    row = db((db.contacts.company==cid)&(db.contacts.primary_contact==1)).select(db.contacts.ALL).first()
    try:
        pc = row.first_name+' '+row.last_name
        title = row.title
        email = row.email
    except:
        pc = 'Add Primary Contact'
        title = 'Add title'
        email = 'Add email'
    db(db.companies.id==cid).update(primary_contact=pc,title=title,email=email)

# -*- coding: utf-8 -*-

me = auth.user_id
dpm = db((db.auth_group.role=='default_project_manager')&(db.auth_membership.user_id>0)).select(db.auth_membership.user_id).first()
if dpm:
    default_project_manager = int(dpm.user_id)

from datetime import datetime,date,timedelta;
today = datetime.today();
yesterday = today - timedelta(days=1);
tomorrow = today + timedelta(days=1);
month = today + timedelta(days=30);
week = today + timedelta(days=7);
date_today = datetime.today().strftime("%m/%d/%Y");
last_week = today - timedelta(days=7);

priorities = ['1','2','3','4','5','6','7','8','9','10']

approvals = ['approved','not-approved']

statuses = ['pending','open','assigned','blocked','on hold','unassigned','completed','deleted','late','closed']

db.define_table('ops_project_types',
                Field('type'),
                auth.signature,
                format='%(type)s'
               )

db.define_table('ops_projects',
                Field('company',db.crm_companies),
                Field('project_type',db.ops_project_types,default=1),
                Field('project_name',length=150,unique=True),
                Field('project_description','text'),
                Field('project_requirements','list:string'),
                Field('start_date','date',default=today),
                Field('due_date','date',default=month),
                Field('date_delivered','date'),
                Field('project_manager','reference auth_user',default=me),
                Field('assigned_to','list:reference auth_user', length=30, default=me),
                Field('created_by',default=me,readable=False,writable=False),
                Field('project_status',requires=IS_IN_SET(statuses),default='pending'),
                Field('approved',requires=IS_IN_SET(approvals),default='approved'),
                Field('project_number',length=150,unique=True,writable=False),
                Field('budget','decimal(15,2)'),
                Field('actual','decimal(15,2)',writable=False),
                auth.signature,
                format='%(project_number)s - %(project_name)s'
               )


db.define_table('ops_project_files',
                Field('project_id',db.ops_projects),
                Field('filename'),
                Field('file_path'),
                auth.signature,
                format='%(filename)s'
               )

db.define_table('ops_tasks',
                Field('project_id','integer',label='Project'),
                Field('user_id','integer',default=me,readable=False,writable=False),
                Field('linked_task_id','integer',default=0),
                Field('task_name'),
                Field('notes','text'),
                Field('objective','text'),
                Field('priority',requires=IS_IN_SET(priorities),default=1),
                Field('assigned_to',db.auth_user,default=me),
                Field('start_date','date',default=today),
                Field('due_date','date',default=week),
                Field('date_delivered','date'),
                Field('time_estimate','decimal(15,2)',default=5,requires=IS_NOT_EMPTY()),
                Field('total_time_logged','decimal(15,2)',default=0),
                Field('tasks_status',requires=IS_IN_SET(statuses),default='open'),
                auth.signature,
                format='%(id)s %(task_name)s'
               )

task_note_types = ['tech note','instructions','critical note','general']

db.define_table('ops_task_notes',
                Field('user_id','integer'),
                Field('task_id','integer'),
                Field('note_type',requires=IS_IN_SET([task_note_types]),default='general',length=150),
                Field('note_title','string',length=50),
                Field('details','text'),
                auth.signature
               )

db.define_table('ops_workspace',
                Field('user_id','integer'),
                Field('planned_worked_time','list:string'),
                Field('workspace_date','date',default=today),
                Field('workspace_tasks','list:integer')
               )

db.define_table('ops_task_items',
                Field('task_id','integer'),
                Field('item_name'),
                Field('item_path'),
                Field('description','text'),
                Field('assigned_to',db.auth_user,default=me),
                Field('priority',requires=IS_IN_SET(priorities)),
                auth.signature
               )

db.define_table('task_dependencies',
                Field('task_id','integer'),
                Field('dependecy_name'),
                Field('item_path'),
                Field('description','text'),
                Field('assigned_to',db.auth_user,default=me),
                Field('priority',requires=IS_IN_SET(priorities)),
                auth.signature
               )

db.define_table('ops_time_card',
                Field('user_id',db.auth_user,default=me,readable=False,writable=False),
                Field('task_id',db.ops_tasks),
                Field('hours','decimal(10,2)'),
                Field('details','text'),
                Field('date_entered','datetime',default=today),
                Field('work_done_on','datetime',default=(today - timedelta(hours=1))),
                auth.signature
               )

g_daily_work_hours = float(7.5)

project_query = (db.ops_projects.project_status=='open')
# Week
# Timecards
g_weekly_work_hours = g_daily_work_hours * 5
g_weekly_hours = []
g_whours = db((db.ops_time_card.user_id==me)&(db.ops_time_card.work_done_on>=last_week)&(db.ops_time_card.work_done_on<=today)).select(db.ops_time_card.hours)
for w in g_whours:
    g_weekly_hours.append(w.hours)
g_weeks_logged_hours = sum(g_weekly_hours)
g_worked_percentage = ((float(g_weeks_logged_hours))/float(g_weekly_work_hours))*100.0
if g_worked_percentage <= 50.0:
    g_wp_color = "#CF3729"
elif g_worked_percentage > 50.0 and g_worked_percentage < 80.0:
    g_wp_color = "#D18B0E"
elif g_worked_percentage >= 80.0:
    g_wp_color = "#008D4C"
# Tasks
g_weekly_time_estimates = []
g_wtasks = db(project_query&(db.ops_projects.id==db.ops_tasks.project_id)&(db.ops_tasks.assigned_to==me)&(db.ops_tasks.due_date<=week)&(db.ops_tasks.due_date>=today)&(db.ops_tasks.tasks_status!='completed')&(db.ops_tasks.tasks_status!='deleted')).select(db.ops_tasks.time_estimate,db.ops_tasks.total_time_logged)
for w in g_wtasks:
    if None == w.time_estimate:
        g_time_estimate = 1
    else:
        g_time_estimate = w.time_estimate - w.total_time_logged
    g_weekly_time_estimates.append(g_time_estimate)
g_weeks_estimated_time = sum(g_weekly_time_estimates)
g_availablity = ((float(g_weeks_estimated_time))/float(g_weekly_work_hours))*100.0
if g_availablity <= 50.0:
    g_av_color = "#008D4C"
elif g_availablity > 50.0 and g_availablity < 80.0:
    g_av_color = "#D18B0E"
elif g_availablity >= 80.0:
    g_av_color = "#CF3729"
# Day
# Tasks
g_daily_time_estimates = []
g_dtasks = db(project_query&(db.ops_projects.id==db.ops_tasks.project_id)&(db.ops_tasks.assigned_to==me)&(db.ops_tasks.due_date<=tomorrow)&(db.ops_tasks.due_date>=today)&(db.ops_tasks.tasks_status!='completed')&(db.ops_tasks.tasks_status!='deleted')).select(db.ops_tasks.time_estimate,db.ops_tasks.total_time_logged)
for d in g_dtasks:
    if None == d.time_estimate:
        g_daily_time_estimate = 1
    else:
        g_daily_time_estimate = d.time_estimate - d.total_time_logged
    g_daily_time_estimates.append(g_daily_time_estimate)
g_days_estimated_time = sum(g_daily_time_estimates)
g_day_availablity = ((float(g_days_estimated_time))/float(g_daily_work_hours))*100.0
if g_day_availablity <= 50.0:
    g_dav_color = "#008D4C"
elif g_day_availablity > 50.0 and g_day_availablity < 80.0:
    g_dav_color = "#D18B0E"
elif g_day_availablity >= 80.0:
    g_dav_color = "#CF3729"
# Timecards
g_daily_hours = []
g_dhours = db((db.ops_time_card.user_id==me)&(db.ops_time_card.work_done_on>=yesterday)&(db.ops_time_card.work_done_on<=today)).select(db.ops_time_card.hours)
for d in g_dhours:
    g_daily_hours.append(d.hours)
g_days_logged_hours = sum(g_daily_hours)
g_daily_worked_percentage = ((float(g_days_logged_hours))/float(g_daily_work_hours))*100.0
if g_daily_worked_percentage <= 50.0:
    g_dp_color = "#CF3729"
elif g_daily_worked_percentage > 50.0 and g_daily_worked_percentage < 80.0:
    g_dp_color = "#D18B0E"
elif g_daily_worked_percentage >= 80.0:
    g_dp_color = "#008D4C"
# Overdue
# Assigned Tasks
g_overdue_times = []
g_odtasks = db(project_query&(db.ops_projects.id==db.ops_tasks.project_id)&(db.ops_tasks.assigned_to==me)&(db.ops_tasks.due_date<today)&(db.ops_tasks.tasks_status!='completed')&(db.ops_tasks.tasks_status!='deleted')).select(db.ops_tasks.time_estimate,db.ops_tasks.total_time_logged)
for o in g_odtasks:
    if None == o.time_estimate:
        g_overdue_time = 1
    else:
        g_overdue_time = o.time_estimate - o.total_time_logged
    g_overdue_times.append(g_overdue_time)
g_overdue_estimated_time = sum(g_overdue_times)
# Delegated Tasks
g_doverdue_times = []
g_dodtasks = db(project_query&(db.ops_projects.id==db.ops_tasks.project_id)&(db.ops_tasks.user_id==me)&(db.ops_tasks.due_date<today)&(db.ops_tasks.tasks_status!='completed')&(db.ops_tasks.tasks_status!='deleted')).select(db.ops_tasks.time_estimate,db.ops_tasks.total_time_logged)
for d in g_dodtasks:
    if None == d.time_estimate:
        g_doverdue_time = 1
    else:
        g_doverdue_time = d.time_estimate - d.total_time_logged
    g_doverdue_times.append(g_doverdue_time)
g_doverdue_estimated_time = sum(g_doverdue_times)

oh = 120 # this is an overhead factor.  Overhad is the total cost of running the busienss per hour.

def update_actuals(pid):
    
    ## this calculates the actual cost based on the time card entries.
    row = db(db.ops_tasks.project_id==pid).select(db.ops_tasks.total_time_logged.sum())
    tl = row.first()[db.ops_tasks.total_time_logged.sum()]
    if tl == None:
        tl = 0

    db(db.ops_projects.id==pid).update(actual=float(tl*oh))
    db.commit()
    
    return float(tl*oh)

def update_budgets(pid):
    
    ## this calculates the budget based on the task time estimates
    row = db(db.ops_tasks.project_id==pid).select(db.ops_tasks.time_estimate.sum())
    te = row.first()[db.ops_tasks.time_estimate.sum()]
    if te == None:
        te = 0

    db(db.ops_projects.id==pid).update(budget=float(te*oh))
    db.commit()
    
    return float(te*oh)

    
def update_project_number(pid): # This code creates the Project Code for Accounting Purposes.
    try:
        row = db(db.ops_projects.id==pid).select(db.ops_projects.id,db.ops_projects.company).first()
        db(db.ops_projects.id==pid).update(project_number=('C%sPR%s')%(row.company,row.id))
    except:
        db(db.ops_projects.id==pid).update(project_number='failed')
    db.commit()

# -*- coding: utf-8 -*-

db.define_table('sim_suppliers',
                Field('company_name'),
                Field('phone'),
                Field('email'),
                Field('contact'),
                Field('website'),
                Field('portal'),
                Field('username'),
                Field('passwd','password'),
                auth.signature,
                format='%(company_name)s'
               )

db.define_table('sim_data_plans',
                Field('sim_supplier',db.sim_suppliers),
                Field('plan_type'),
                auth.signature,
                format='%(plan_type)s'
               )

db.define_table('cell_networks',
                Field('company_name'),
                Field('frequencies'),
                Field('coverage'),
                auth.signature,
                format='%(company_name)s'
               )

db.define_table('sims',
                Field('sim_number',length=150,unique=True),
                Field('static_ip'),
                Field('supplier',db.sim_suppliers),
                Field('network_id',db.cell_networks),
                auth.signature,
                format='%(sim_number)s'
               )

db.define_table('active_sims',
                Field('sim_id',db.sims,unique=True),
                Field('customer_id',db.crm_companies),
                Field('data_plan',db.sim_data_plans),
                Field('startdate','date'),
                Field('billing_cycle'),
                Field('renewal_date','date'),
                auth.signature
               )

# -*- coding: utf-8 -*-

db.define_table('user_profile',
                Field('user_id','integer'),
                Field('name','string'),
                Field('image', 'string'),
                Field('image_path'),
                Field('uploaded_by','integer'),
                Field('uploaded_on','datetime'),
                format='%(name)s')

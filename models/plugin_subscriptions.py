# -*- coding: utf-8 -*-

db.define_table('subscriptions',
                Field('sub_name'),
                Field('sub_type',requires=IS_IN_SET(['support','saas','license'])),
                Field('price'),
                Field('frequency',requires=IS_IN_SET(['annually','monthly','prepetual'])),
                auth.signature,
                format='%(sub_name)s'
               )

db.define_table('active_subs',
                Field('customer_id',db.crm_companies),
                Field('sub_id',db.subscriptions),
                Field('start_date','date'),
                Field('renewal_date','date')
               )

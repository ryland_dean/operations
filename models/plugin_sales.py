from datetime import datetime, timedelta,date
import time
now = datetime.today()
today = date.today()
four_weeks = today + timedelta(weeks=4)
me = auth.user_id

STAGES = ['prospect','lead','quote','booked','invoiced','completed','dead']

CURRENCIES = ['CAN','USD']

db.define_table('opportunities',
                Field('company_id',db.crm_companies),
                Field('account_manager_id','integer'), # the list of account managers can be found in the "sales" membership group.
                Field('description','text'),
                Field('estimated_value'),
                Field('stage',requires=IS_IN_SET(STAGES),default='prospect'),
                Field('target_close_date','date',default=four_weeks), # the default cloing date is 4 weeks after the date added by default.
                auth.signature
               )

db.define_table('opportunity_notes',
                Field('opportunity_id',db.opportunities),
                Field('user_id','integer',default=me), # this captures the user id of the user that adds the comments.
                Field('note','text'),
                auth.signature
               )

# Add a record here everytime the opporunity changes stages.
db.define_table('opportunity_history',
                Field('o_id','integer'),
                Field('stage'),
                Field('date_changed',default=today),
                Field('user_id',default=me),
                auth.signature
               )

db.define_table('quotes',
                Field('company_id',db.crm_companies),
                Field('contact_id',db.crm_contacts),
                Field('project_id',db.ops_projects),
                Field('quote_number'),
                Field('quote_title'),
                Field('date_added','datetime'),
                Field('quote_status',requires=IS_IN_SET(['open','closed']),default='open'),
                Field('score','integer',default=20), # Score is the cofidence level of the success of the sale.
                Field('quote_total','decimal(15,2)'),
                auth.signature,
                format='%(quote_title)s')

db.quotes.id.represent = lambda id, field: \
    A(id, _href=URL(c="default", f="quotes", args=["quotes", id]))

db.define_table('quote_details',
                Field('quote_id','reference quotes', default='session.quote_id'),
                Field('product_id','integer'),
                Field('qty','integer',default=0),
                Field('price','decimal(15,2)',default=0),
                Field('discount','decimal(15,2)',default=0),
                Field('line_total','decimal(15,2)',default=0),
                Field('comments','text')
               )

db.define_table('sales',
                Field('sale_name'),
                Field('sale_description'),
                Field('sale_stage',requires=IS_IN_SET(STAGES),default='opportunity'),
                format='%(sale_name)s')

db.define_table('trips',
                Field('departure_date','date'),
                Field('return_date','date'),
                Field('user_id'),
                Field('company',db.crm_companies),
                Field('opportunity_id',db.opportunities),
                Field('sale_name',db.sales),
                Field('trip_objective','text'),
                Field('trip_report','text'),
                auth.signature)
